window.addEventListener('DOMContentLoaded', () => {
	const keys = document.getElementsByClassName('keys')[0];
	const text = document.querySelector('.display > input');
	const equal = document.getElementById('equal');
	//console.dir(equal);
	let calc = {
		op1: '',
		op2: '',
		op3: '',
		mr: 0
	};

	function calculate (calc) {
		let a = parseFloat(calc.op1);
		let b = parseFloat(calc.op3);

		switch (calc.op2) {
			case '+':
				return a + b;
			case '-':
				return a - b;
			case '*':
				return a * b;
			case '/':
				return a / b;
		}
	}

	function clear() {
		text.value = '';
		equal.disabled = true;
		calc.op1 = '';
		calc.op2 = '';
		calc.op3 = '';
	}

	keys.addEventListener('click', function (e) {
		if (e.target.value) {
			if (e.target.value.search(/[0-9.]/) != -1) {
				if (calc.op2 == '') {
					calc.op1 += e.target.value;
					text.value = calc.op1;
				} else {
					calc.op3 += e.target.value;
					text.value = calc.op3;
					equal.disabled = false;
				}
				return;
			}
			if (e.target.value.search(/[-+*/]/) == 0) {
				if (calc.op1 != '') {
					calc.op2 = e.target.value;
				} else if (text.value != '') {
					calc.op1 = text.value;
					calc.op2 = e.target.value;
				}
				console.log(calc);
				return;
			}
			if (e.target.value.search(/m\+/) != -1) {
				calc.mr += parseFloat(text.value);
				return;
			}
			if (e.target.value.search(/m\-/) != -1) {
				calc.mr -= parseFloat(text.value);
				return;
			}
			if (e.target.value.search('=') != -1) {
				let result = calculate(calc);
				clear();
				text.value = result.toString();
				//calc.op1 = result;
				return;
			}
			if (e.target.value.search('C') != -1) {
				clear();
				return;
			}
			if (e.target.value.search('mrc') != -1) {
				text.value = calc.mr;
				calc.mr = 0;
			}
		}		
	});
})
